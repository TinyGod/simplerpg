﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class DamageReceiver : MonoBehaviour
    {

        [Header("REFERENCES")]
        public PlayerStats PlayerStats;

        [Header("VARIABLES")]
        public int n;

        [Header("FLAGS")]
        public bool tf;

        void Start()
        {
            PlayerStats = GetComponent<PlayerStats>();
        }

        void Update()
        {

        }

        public void TakeDamage(DamageInfo DamageInfo)
        {

            var RealDamageTaken = DamageInfo.BaseDamage;

            switch (DamageInfo.DamageElement)
            {
                case ElementalDamageType.Fire:
                    RealDamageTaken -= PlayerStats.FireResistance;
                    break;
            }

            PlayerStats.RemoveHP(RealDamageTaken);
        }
    }
}