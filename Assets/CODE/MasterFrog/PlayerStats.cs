﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


namespace UnityCommunityProject
{
    public class PlayerStats : MonoBehaviour
    {

        [Header("REFERENCES")]
        public GameObject go;

        [Header("VARIABLES")]
        [SerializeField]
        int HP_MAX = 100;
        int HP_Current;
        int Level = 1;
        int CurrentLevelExp = 0;
        // How much the player has accumulated since the last level
        int NextLevelExp = 10;
        // How much experience the player needs to gain the next level

        // Elemental Resistances
        public int FireResistance;

        [Header("FLAGS")]
        public bool tf;

        public event Action<GameObject> OnDeath;

        void Start()
        {
            GiveFullStats();
            OnDeath += DestroyOnDeath;
        }

        public void GiveFullStats()
        {
            HP_Current = HP_MAX;
        }

        public int GetCurrentHP()
        {
            return (HP_Current);
        }

        public int GetMaximumHP()
        {
            return (HP_MAX);
        }

        public int GetCurrentLevelExp()
        {
            return (CurrentLevelExp);
        }

        public int GetNextLevelExp()
        {
            return (NextLevelExp);
        }

        public int GetCurrentLevel()
        {
            return (Level);
        }

        public void RemoveHP(int amount)
        {
            HP_Current -= amount;
            if (HP_Current <= 0)
            {
                if (OnDeath != null)
                    OnDeath(gameObject);
            }
        }

        private void DestroyOnDeath(GameObject killedGameObject)
        {
            Debug.Log(name + " has died, blah.");
            Destroy(killedGameObject);
        }
    }
}