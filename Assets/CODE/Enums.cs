﻿namespace UnityCommunityProject
{
    public enum CameraType
    {
        Undefined = -1,
        MapGeneration = 0,
        TopDown = 1,
        ThirdPerson = 2,
        FirstPerson = 3,
        InSaucerView = 4
    }

    public enum ColliderType
    {
        Undefined = -1,
        OuterZone = 0,
        InnerZone = 1
    }

    public enum MessageType
    {
        Undefined = -1,
        NearStructure = 0,
        NoLongerNearStructure = 1,
        EnteredBuilding = 2,
        ExitedBuilding = 3,

        // Gar 04/02/2017 Need to let Camera know these events
        PlayerEnteredSaucer = 4,
        PlayerExitedSaucer = 5
    }

    public enum ObjectType
    {
        Undefined = -1,
        Building = 0,
        ResourceStructure = 1
    }

    public enum ElementalDamageType
    {
        Neutral = 0,
        Fire = 1
    }
}