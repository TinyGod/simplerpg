﻿using UnityEngine;


namespace UnityCommunityProject
{
    // I can stick this on any object to provide some basic information such as type of the object, name and description
    public class ObjectInfo : MonoBehaviour
    {
        // hard-wire reference in the Inspector
        public GameObject roof;

        // configure in the Inspector
        public ObjectType _type;
        public string _name;
        public string _description;

        // just a convenience method if ever I want to display the type of the object
        public string TypeAsString()
        {
            return _type.ToString();
        }

        // just a bit of basic control of the roof visibility here
        public void SetRoofVisible(bool visible)
        {
            roof.SetActive(visible);
        }
    }
}