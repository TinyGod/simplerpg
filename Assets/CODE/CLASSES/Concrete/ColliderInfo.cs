﻿using UnityEngine;


namespace UnityCommunityProject
{
    // I can stick this on any collider GameObject to set which type of Collider it is
    // For now this is only used to determine Outer and Inner Zones for walking around outside structures and inside buildings
    public class ColliderInfo : MonoBehaviour
    {
        // configure in the Inspector
        public ColliderType _type;

        // public for hard-wiring in the Inspector
        public ObjectInfo _object;

        void OnTriggerEnter(Collider otherCollider)
        {
            // if the object that set off this trigger is not a ColliderMessageReceiver there is nothing more to do
            IColliderMessageReceiver receiver = otherCollider.gameObject.GetComponent<IColliderMessageReceiver>();
            if (receiver == null)
                return;

            // create the message object & populate the descriptors information
            Message message = CreateMessage();

            // set the specifics of this message... which is simply the exact message type
            switch (this._type)
            {
                case ColliderType.OuterZone:
                    switch (_object._type)
                    {
                        case ObjectType.Building:
                        case ObjectType.ResourceStructure:
                            message.mtype = MessageType.NearStructure;
                            break;
                    }
                    break;

                case ColliderType.InnerZone:
                    switch (_object._type)
                    {
                        case ObjectType.Building:
                            message.mtype = MessageType.EnteredBuilding;

                            // not crazy about doing this here but for now it works... entered building so hide the roof
                            _object.SetRoofVisible(false);
                            break;
                    }
                    break;
            }

            // send the message
            receiver.ReceiveColliderMessage(message);
        }

        void OnTriggerExit(Collider otherCollider)
        {
            // if the object that set off this trigger is not a ColliderMessageReceiver there is nothing more to do
            IColliderMessageReceiver receiver = otherCollider.gameObject.GetComponent<IColliderMessageReceiver>();
            if (receiver == null)
                return;

            // create the message object & populate the descriptors information
            Message message = CreateMessage();

            // set the specifics of this message... which is simply the exact message type
            switch (this._type)
            {
                case ColliderType.OuterZone:
                    switch (_object._type)
                    {
                        case ObjectType.Building:
                        case ObjectType.ResourceStructure:
                            message.mtype = MessageType.NoLongerNearStructure;
                            break;
                    }
                    break;

                case ColliderType.InnerZone:
                    switch (_object._type)
                    {
                        case ObjectType.Building:
                            message.mtype = MessageType.ExitedBuilding;

                            // not crazy about doing this here but for now it works... exited building so show the roof
                            _object.SetRoofVisible(true);
                            break;
                    }

                    break;
            }

            // send the message
            receiver.ReceiveColliderMessage(message);
        }

        // creates a new message & populates the descriptors information for it
        private Message CreateMessage()
        {
            Message message = new Message();
            message.name = _object._name;
            message.description = _object._description;
            message.otype = _object._type;

            return message;
        }
    }
}
