﻿using System;
using UnityEngine;


namespace UnityCommunityProject
{
    public class FollowPlayer1 : MonoBehaviour, IMessageReceiver
    {
        // hard-wire this reference in the Inspector
        public CameraType _cameratype;
        public Transform _player;
        public bool UseUpdateInsteadOfLateUpdate;

        private Vector3 _offset;
        private Vector3 _cameralookat;
        private Vector3[] _offsetsforcameratypes = new Vector3[] {
            new Vector3(0.0f, 959.0f, 0.0f),    // Map Generation
            new Vector3(0.0f, 19.0f, -3.0f),    // Top Down
            new Vector3(-0.3f, 11.0f, -12.0f),  // Third Person
            new Vector3(0f, 2f, 0f),            // First Person
            new Vector3(0.0f, 10.0f, 0.0f),    // In Saucer View
        };

        // when player enters UFO (could be used if we keep the entering of buildings as well) the current camera view
        // is saved in this variable and will be restored when the player exits the UFO.
        private CameraType _previouscameratype;

        void Start()
        {
            if (_cameratype == CameraType.Undefined)
                _cameratype = CameraType.ThirdPerson;

            _offset = _offsetsforcameratypes[(int)_cameratype];

            // Gar 04/02/2017 I used this to generate the camera offsets just copied from log and pasted in _offsetsforcameratypes above
            //_offset = transform.position - _player.position;
            //Debug.Log("Camera offset = " + _offset);
        }


        void Update()
        {
            if (!UseUpdateInsteadOfLateUpdate)
                return;

            float angle = _player.transform.eulerAngles.y;
            Quaternion rotation = Quaternion.Euler(0, angle, 0);
            transform.position = _player.transform.position + (rotation * _offset);

            if (_cameratype != CameraType.FirstPerson)
                transform.LookAt(_player.transform);
            else
            {
                _cameralookat = _player.transform.position + (_player.transform.forward * 10f);
                _cameralookat.y += 0.5f;
                transform.LookAt(_cameralookat);
            }
        }

        void LateUpdate()
        {
            if (UseUpdateInsteadOfLateUpdate)
                return;

            float angle = _player.transform.eulerAngles.y;
            Quaternion rotation = Quaternion.Euler(0, angle, 0);
            transform.position = _player.transform.position + (rotation * _offset);

            if (_cameratype != CameraType.FirstPerson)
                transform.LookAt(_player.transform);
            else
            {
                _cameralookat = _player.transform.position + (_player.transform.forward * 10f);
                _cameralookat.y += 0.5f;
                transform.LookAt(_cameralookat);
            }
        }


        #region IMessageReceiver Implementation: Message Handling

        // receives messages and sorts them out
        public void ReceiveMessage(Message thisMessage)
        {
            switch (thisMessage.mtype)
            {
                case MessageType.PlayerEnteredSaucer:
                    // Not sure but seems like this message is coming in twice in my test so adding this check
                    if (_cameratype != CameraType.InSaucerView)
                    {
                        _previouscameratype = _cameratype;
                        _cameratype = CameraType.InSaucerView;
                        _offset = _offsetsforcameratypes[(int)_cameratype];
                    }
                    break;

                case MessageType.PlayerExitedSaucer:
                    _cameratype = _previouscameratype;
                    //_cameratype = CameraType.FirstPerson;
                    _offset = _offsetsforcameratypes[(int)_cameratype];
                    break;
            }
        }

        #endregion


    }
}