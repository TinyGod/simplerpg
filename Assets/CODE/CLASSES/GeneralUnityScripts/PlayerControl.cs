﻿using UnityEngine;


namespace UnityCommunityProject
{
    public class PlayerControl : MonoBehaviour, IColliderMessageReceiver
    {
        // configure in the Inspector
        public float _walkmovespeed;
        public float _walkanimspeed;
        public float _turnspeed;

        private string _textmessage;
        private Rigidbody _rigidbody;
        private Transform _transform;

        public bool AI_Controlled;
        public AI_Controller AI_Controller;

        // just a bit of tracking code here because depending on positioning of zone colliders it is possible for
        // a Near (a new area) message to be sent BEFORE the NoLongerNear (the previous area) message is sent.
        // this stuff should probably be handled by simply fixing the collider size or positions in the Editor
        // but the code to hand it is simple so I will leave it
        private Message _lastMessage = null;

        private float[] _simplewalkAnim = new[] { 1f, 0.98f, 0.96f, 0.94f, 0.92f, 0.90f, 0.92f, 0.94f, 0.96f, 0.98f };
        private float _walkframe;


        #region IColliderMessageReceiver Implementation: Message Handling

        // receives messages and sorts them out
        public void ReceiveColliderMessage(Message thisMessage)
        {
            switch (thisMessage.mtype)
            {
                case MessageType.EnteredBuilding:
                    _textmessage = "Player entered the " + thisMessage.name;
                    Debug.Log(_textmessage);
                    break;

                case MessageType.ExitedBuilding:
                    _textmessage = "Player exited the " + thisMessage.name;
                    Debug.Log(_textmessage);
                    break;

                case MessageType.NearStructure:
                    // this just keeps the messages in the correct order. Probably should route the messages
                    // through a central message handler and sort it out there but it works here so...
                    // if we get two NearStructure messages in a row we know we need to throw out the next NoLongerNearStructure message
                    // for the old place
                    if (_lastMessage != null)
                    {
                        if (_lastMessage.mtype == MessageType.NearStructure)
                        {
                            // handle leaving last area
                            _textmessage = "Player is no longer near the " + _lastMessage.name;
                            Debug.Log(_textmessage);
                        }
                    }

                    _lastMessage = thisMessage;

                    _textmessage = "Player is near the " + thisMessage.name;
                    Debug.Log(_textmessage);
                    break;

                case MessageType.NoLongerNearStructure:
                    // same as above only filtering out the NoLongerNear message that came in "after" the Near new area message
                    if (_lastMessage != null)
                    {
                        if (_lastMessage.mtype == MessageType.NearStructure && _lastMessage.name != thisMessage.name)
                        {
                            _lastMessage = null;
                            return;
                        }
                        if (_lastMessage.mtype == MessageType.NoLongerNearStructure && _lastMessage.name == thisMessage.name)
                        {
                            _lastMessage = null;
                            return;
                        }
                    }

                    _lastMessage = thisMessage;

                    _textmessage = "Player is no longer near the " + thisMessage.name;
                    Debug.Log(_textmessage);
                    break;
            }
        }

        #endregion






        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _transform = GetComponent<Transform>();
        }

        void Update()
        {
            Vector3 v3Pos = transform.position;
            Vector3 v3Dist;

            if (!AI_Controlled)
            {

                v3Dist.y = 0f;
                v3Dist.x = Input.GetAxis("Horizontal");
                v3Dist.z = Input.GetAxis("Vertical");
                v3Dist.Normalize();

                if (v3Dist.x < 0f)
                    _transform.Rotate(Vector3.up, -_turnspeed * Time.deltaTime);
                else if (v3Dist.x > 0f)
                    _transform.Rotate(Vector3.up, _turnspeed * Time.deltaTime);

                if (v3Dist.z != 0f)
                {
                    v3Pos += transform.forward * v3Dist.z * _walkmovespeed * Time.deltaTime;
                    _rigidbody.MovePosition(v3Pos);
                }
            }
            else {
                v3Dist.y = 0f;
                v3Dist.x = AI_Controller.GetHorizontalAxis();
                v3Dist.z = AI_Controller.GetVerticalAxis();
                v3Dist.Normalize();
                v3Pos += v3Dist * _walkmovespeed * Time.deltaTime;
                _rigidbody.MovePosition(v3Pos);
            }

            if (v3Dist.x == 0f && v3Dist.z == 0f)
            {
                // not moving set player imagery back to normal
                _transform.localScale = new Vector3(1f, 1f, 1f);
                _walkframe = 0f;
                return;
            }

            // simple animation... yeah I know I am not using the animator but Lordy this stuff is so
            // simple I cannot bring myself to go through all of that hassle just to do a simple animation!
            _walkframe += _walkanimspeed * Time.deltaTime;
            if (_walkframe >= _simplewalkAnim.Length)
                _walkframe = 0f;

            _transform.localScale = new Vector3(_simplewalkAnim[(int)_walkframe], 1f, _simplewalkAnim[(int)_walkframe]);
        }

    }
}

