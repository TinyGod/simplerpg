﻿using UnityEngine;
using System.Collections;

namespace DanielQuick {

	/// <summary>
	/// Meant to be the hub of the Player. This allows any object in the world has an easy way to find the player reference
	/// </summary>
	public class Player : MonoBehaviour {

		private static Player instance;
		public static Player Instance {
			get {
				return instance;
			}
		}

		void Awake() {
			instance = this;
		}

	}

}