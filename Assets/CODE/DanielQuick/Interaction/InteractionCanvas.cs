﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionCanvas : MonoBehaviour {

	[SerializeField]
	private Animator iconPrefab;

	private Dictionary<Transform, Animator> icons;
	private Stack<Animator> pool;

	private static InteractionCanvas instance;
	public static InteractionCanvas Instance {
		get {
			return instance;
		}
	}

	void Awake() {
		instance = this;
		icons = new Dictionary<Transform, Animator>();
		pool = new Stack<Animator>();
	}

	void LateUpdate() {
		foreach(Transform anchor in icons.Keys) {
			Animator iconAnimator = icons[anchor];
			Vector3 direction = anchor.position - Camera.main.transform.position;

			if(Vector3.Dot(Camera.main.transform.forward, direction) > 0) {
				iconAnimator.transform.position = Camera.main.WorldToScreenPoint(anchor.position);
			} else {
				iconAnimator.transform.position = -Vector3.one * 9999;
			}
		}
	}

	public void AddIcon(Transform anchor, KeyCode keyCode) {
		Animator animator;

		if(icons.ContainsKey(anchor)) {
			animator = icons[anchor];
		} else {
			animator = GetNewIcon();
			icons.Add(anchor, animator);
		}

		animator.transform.Find("Backing/Text").GetComponent<Text>().text = KeyCodeToString(keyCode);
		animator.SetBool("IsActive", true);
	}

	public void RemoveIcon(Transform anchor) {
		if(icons.ContainsKey(anchor)) {
			Animator animator = icons[anchor];
			StartCoroutine(AddToPoolDelayed(anchor, animator));
			animator.SetBool("IsActive", false);
			animator.SetFloat("DeactivationTime", Time.time);
		}
	}

	public void ActivateIcon(Transform anchor) {
		if(icons.ContainsKey(anchor)) {
			icons[anchor].SetTrigger("Activate");
			RemoveIcon(anchor);
		}
	}

	IEnumerator AddToPoolDelayed(Transform anchor, Animator animator, float delay = 5f) {
		yield return new WaitForSeconds(delay);
		if(icons.ContainsKey(anchor) &&
			animator.GetFloat("DeactivationTime") < Time.time - (delay - 0.5f) &&
			animator.GetBool("IsActive") == false) {

			icons.Remove(anchor);
			pool.Push(animator);
		}
	}

	Animator GetNewIcon() {
		Animator animator;

		if(pool.Count > 0) {
			animator = pool.Pop();
		} else {
			animator = Instantiate<Animator>(iconPrefab);
			animator.transform.SetParent(transform, false);
		}

		return animator;
	}

	string KeyCodeToString(KeyCode keyCode) {
		char letter = (char)keyCode;
		return letter.ToString().ToUpper();
	}

}
