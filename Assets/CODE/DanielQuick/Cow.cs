﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour, IAbductable {

	private Animator animator;
	private new Rigidbody rigidbody;

	void Awake() {
		animator = GetComponent<Animator>();
		rigidbody = GetComponent<Rigidbody>();
	}

	public void OnStartAbduction() {
		animator.SetBool("IsAbducting", true);
		rigidbody.isKinematic = true;
	}

	public void OnCancelAbduction() {
		animator.SetBool("IsAbducting", false);
		rigidbody.isKinematic = false;
	}

	public void OnSuccessfulAbduction() {
		Destroy(gameObject);
	}

}
