﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityCommunityProject;
using System;

namespace DanielQuick {

	public class UFO : MonoBehaviour, IInteract {

		[SerializeField]
		InputInteractTrigger trigger;

		[SerializeField]
		private Transform playerParent;
		[SerializeField]
		private Transform exitPosition;

		private TractorBeam tractorBeam;

		private UFOLocomotion locomotion;

		private UFOStatus status;

		public UFOStatus Status {
			get {
				return status;
			}
		}

		void Awake() {
			locomotion = GetComponent<UFOLocomotion>();
			tractorBeam = transform.GetComponentInChildren<TractorBeam>();
		}

		public void Interact() {
			status = UFOStatus.Lifting;
			locomotion.LiftOff(OnFlying);
			Player.Instance.GetComponent<Rigidbody>().isKinematic = true;
			Player.Instance.transform.SetParent(playerParent);
			Player.Instance.transform.localPosition = Vector3.zero;
			Player.Instance.GetComponent<PlayerControl>().enabled = false;

			Camera.main.GetComponent<FollowPlayer1>().enabled = false;
			Camera.main.GetComponent<UFOCamera>().Initialize(transform);
			Camera.main.GetComponent<UFOCamera>().enabled = true;

            // Gar 04/02/2017
//            NotifyCameraPlayerEnteredUFO();
        }

        void OnFlying() {
			status = UFOStatus.Flying;
        }

        void OnGrounded() {
			status = UFOStatus.Grounded;
			Player.Instance.GetComponent<Rigidbody>().isKinematic = false;
			Player.Instance.transform.SetParent(null);
			Player.Instance.transform.position = exitPosition.position;
			Player.Instance.GetComponent<PlayerControl>().enabled = true;
			trigger.Reset();

			Camera.main.GetComponent<FollowPlayer1>().enabled = true;
			Camera.main.GetComponent<UFOCamera>().enabled = false;

            // 04/02/2017
//            NotifyCameraPlayerExitedUFO();
        }

        public void EndInteract() {
        }

        void ExitUFO() {
			status = UFOStatus.Landing;
			locomotion.Land(OnGrounded);
        }

		void Update() {
			if(status == UFOStatus.Flying) {
				if(Input.GetKeyDown(KeyCode.Space)) {
					tractorBeam.Activate();
				}
				if(Input.GetKeyUp(KeyCode.Space)) {
					tractorBeam.Deactivate();
				}

				if(Input.GetKeyDown(KeyCode.E)) {
					ExitUFO();
				}
			}
		}


        // Gar 04/02/2017
        private void NotifyCameraPlayerEnteredUFO()
        {
            // Let the camera know what is going on
            // if the object that set off this trigger is not a ColliderMessageReceiver there is nothing more to do
            IMessageReceiver receiver = GameObject.Find("Main Camera").GetComponent<IMessageReceiver>();
            if (receiver == null)
                return;

            // create the message object & populate the descriptors information
            Message message = new Message();
            message.mtype = MessageType.PlayerEnteredSaucer;

            // send message
            receiver.ReceiveMessage(message);
        }

        // Gar 04/02/2017
        private void NotifyCameraPlayerExitedUFO()
        {
            // Let the camera know what is going on
            // if the object that set off this trigger is not a ColliderMessageReceiver there is nothing more to do
            IMessageReceiver receiver = GameObject.Find("Main Camera").GetComponent<IMessageReceiver>();
            if (receiver == null)
                return;

            // create the message object & populate the descriptors information
            Message message = new Message();
            message.mtype = MessageType.PlayerExitedSaucer;

            // send message
            receiver.ReceiveMessage(message);
        }


    }

    public enum UFOStatus {
		Grounded,
		Lifting,
		Flying,
		Landing
	}


}