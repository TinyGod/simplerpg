﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOCamera : MonoBehaviour {

	[SerializeField]
	private float orbitDistance = 10f;
	[SerializeField]
	private float orbitSpeed = 1f;

	[SerializeField]
	private float yMin = 10;
	[SerializeField]
	private float yMax = 89;

	private Transform target;
	private float x = 0, y = 0;

	public void Initialize(Transform target) {
		this.target = target;
		x = transform.eulerAngles.y;
		y = 40;
	}

	void LateUpdate() {
		x += Input.GetAxis("Mouse X") * orbitSpeed;
		y -= Input.GetAxis("Mouse Y") * orbitSpeed;

		y = ClampAngle(y, yMin, yMax);

		Quaternion rotation = Quaternion.Euler(y, x, 0);

		Vector3 position = target.position + rotation * (-Vector3.forward * orbitDistance);

		transform.position = position;
		transform.rotation = rotation;
	}

	private float ClampAngle(float angle, float min, float max) {
		if (angle < -360f)
			angle += 360f;
		if (angle > 360f)
			angle -= 360f;
		return Mathf.Clamp(angle, min, max);
	}

}
