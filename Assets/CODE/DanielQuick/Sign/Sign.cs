﻿using UnityEngine;
using System.Collections;

namespace DanielQuick {

	/// <summary>
	/// Used to display a message onto the UI, utilizing SignCanvas
	/// Activates when player enters the InteractTrigger
	/// </summary>
	public class Sign : MonoBehaviour, IInteract {

		[SerializeField][Multiline]
		private string message;

		[SerializeField]
		private bool showInBuilds = true;

		[SerializeField]
		private InputInteractTrigger trigger;

		private bool isInteracting = false;
		private int interactFrame = 0;

		void Awake() {
			if(!Application.isEditor && !showInBuilds) {
				Destroy(gameObject);
			}
		}

		public void Interact() {
			isInteracting = true;
			interactFrame = Time.frameCount;
			SignCanvas.Instance.ShowMessage(message);
		}

		public void EndInteract() {
			isInteracting = false;
			SignCanvas.Instance.HideMessage(message);
		}

		void Update() {
			if(isInteracting && Time.frameCount != interactFrame) {
				if(Input.GetKeyDown(KeyCode.E)) {
					EndInteract();
					trigger.Reset();
				}
			}
		}

	}

}