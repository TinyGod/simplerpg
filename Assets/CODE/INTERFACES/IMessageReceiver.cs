﻿// Designing this "on the fly" at this point and right way now I am thinking I will separate out
// the messaging interfaces. There shouldn't be many of them anyway.
//
// This messaging interface is for messages sent by Collider GameObjects and is mainly used for identifying
// when the player has moved near an object, away from an object, entered a building and exited a building
// Mainly because I just want to know. I can use this to set atmospheric sound fx and trigger events accordingly
// Events being whatever... maybe the Inn Keeper or Blacksmith greets the player or bids them farewell.
// Maybe when entering a certain zone enemies are alerted, etc. Can be used for many things.
// Of course, those NPCs aren't actually in yet. :)

namespace UnityCommunityProject
{
    // just an easy way to pass messages around
    public interface IColliderMessageReceiver
    {
        void ReceiveColliderMessage(Message message);
    }

    // messages related to the UFO... done this way because I like things logically clean instead of BIG generic stuff handling everything
    public interface IMessageReceiver
    {
        void ReceiveMessage(Message message);
    }
}