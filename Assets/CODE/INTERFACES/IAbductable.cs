﻿public interface IAbductable {
	void OnStartAbduction();
	void OnCancelAbduction();
	void OnSuccessfulAbduction();
}
